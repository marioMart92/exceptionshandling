package exceptionHandling;

import java.util.Scanner;

public class ExceptionHandlingArea {
	// Variables
	private double area;
	private double width;
	private double height;

	Scanner input = new Scanner(System.in);

	// Create getter and setter methods
	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public static void main(String[] args) {

	}

}
